## Description: 

Goal: allow for people to give and receive feedback in a relatively fuss-free (no registration needed) way.


    Use cases:
      - User can create a number of Inboxes. Each Inbox has:
        - a leading question, topic (e.g. "What do you like about me?"),
        - a unique, non-sequential ID,
        - Inbox Owner information in a form of a signature,
          an expiration date after which the Inbox cannot be responded to,
        - a flag allowing for anonymous submissions; if flag is not set, only
          signed messages should be accepted.
      - User can view the Inbox information (topic, owner signature, expiration
        date), but not the replies.
      - User can reply to an Inbox. Each message has:
        - a body,
        - a timestamp,
        - a signature (or no signature if the message is anonymous).
      - Inbox Owner can read Inbox replies after providing credentials matching
        Inbox's signature.
      - Inbox Owner can change the topic of an Inbox, but only before any
        replies are sent to it.


    Signatures are strings generated according to the following scheme:
        <username> + <separator> + <cryptographic hash of username + secret + salt>
    Such signatures are sometimes called "tripcodes".


    TODOs:
        - Implement a domain model based on the above requirements.
          Your domain model should aim for completeness and purity, unless you
          see no other way of achieving the stated business goals. Think
          carefully about how you will expose your domain model to the rest of the
          application.
        - Define interfaces (ports) for any required infrastructural services and
          provide simple implementations (adapters) for these services.
        - Implement a RESTful API utilizing your domain model.
        - Write tests for the implemented functionality.


## Usage:

> docker-compose up

application will be accessible at host/port specified in `.env` file

## Tests

> docker-compose exec app python -m unittest