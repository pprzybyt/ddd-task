import hashlib

salt = hashlib.md5().hexdigest()


def generate_tripcode(username, password, separator='#'):
    signature = hashlib.sha256((username + password + salt).encode('utf-8')).hexdigest()
    return f'{username}{separator}{signature}'


def confirm_tripcode(username, password, signature):
    return generate_tripcode(username, password) == signature
