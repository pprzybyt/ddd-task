import abc
from typing import List, Optional

from inboxes.domain import model
from inboxes import schema
from inboxes.domain import exceptions


class AbstractRepository(abc.ABC):

    @abc.abstractmethod
    def all(self) -> List[schema.Inbox]:
        pass

    @abc.abstractmethod
    def create(self, inbox: schema.InboxCreate, signature: str) -> schema.Inbox:
        pass

    @abc.abstractmethod
    def detail(self, inbox_id: str) -> schema.InboxDetail:
        pass

    @abc.abstractmethod
    def update_topic(self, inbox_id: str, new_topic: str) -> schema.Inbox:
        pass

    @abc.abstractmethod
    def answer(self, inbox_id: str, message: schema.Message, signature: Optional[str]) -> Optional[schema.Message]:
        pass


class DummyRepository(AbstractRepository):

    def __init__(self):
        self._inboxes: List[model.Inbox] = []

    def _get(self, inbox_id: str) -> model.Inbox:
        inbox = next(iter(inbox for inbox in self._inboxes if inbox.id == inbox_id), None)
        if not inbox:
            raise exceptions.InboxNotFound
        return inbox

    def all(self) -> List[schema.Inbox]:
        return [schema.Inbox.parse_obj(vars(inbox)) for inbox in self._inboxes]

    def create(self, inbox: schema.InboxCreate, signature: str) -> schema.Inbox:
        inbox = model.Inbox(**inbox.dict(), signature=signature)
        self._inboxes.append(inbox)
        return schema.Inbox.parse_obj(vars(inbox))

    def detail(self, inbox_id: str) -> schema.InboxDetail:
        inbox = self._get(inbox_id)
        return schema.InboxDetail.parse_obj(vars(inbox))

    def update_topic(self, inbox_id: str, new_topic: str) -> schema.Inbox:
        inbox = self._get(inbox_id)
        inbox.update_topic(new_topic)
        return schema.Inbox.parse_obj(vars(inbox))

    def answer(self, inbox_id: str, message: schema.Message, signature: Optional[str]) -> Optional[schema.Message]:
        inbox = self._get(inbox_id)
        message = schema.Message(**message.dict(), signature=signature)
        inbox.add_message(message)
        return schema.Message.parse_obj(vars(message))

