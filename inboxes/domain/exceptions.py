
class InboxInProgressException(Exception):
    pass


class InboxExpiredException(Exception):
    pass


class NoSignatureException(Exception):
    pass


class InboxNotFound(Exception):
    pass
