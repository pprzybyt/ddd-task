from datetime import datetime
import uuid
from typing import List

from inboxes.domain import exceptions
from inboxes.schema import Message


class Inbox:
    topic: str
    signature: str
    is_anonymous: bool
    expiry_date: datetime
    id: str
    messages: List[Message]

    def __init__(self, topic: str, is_anonymous: bool, expiry_date: datetime, signature: str):
        self.topic = topic
        self.is_anonymous = is_anonymous
        self.expiry_date = expiry_date
        self.id = self._generate_id()
        self.signature = signature
        self.messages = []

    @staticmethod
    def _generate_id():
        return str(uuid.uuid4())

    def update_topic(self, new_topic: str):
        if not self.messages:
            self.topic = new_topic
        else:
            raise exceptions.InboxInProgressException

    def add_message(self, message: Message):
        if datetime.now(tz=self.expiry_date.tzinfo) > self.expiry_date:
            raise exceptions.InboxExpiredException
        if not self.is_anonymous and not message.signature:
            raise exceptions.NoSignatureException
        self.messages.append(message)
