from typing import List, Optional

from fastapi import FastAPI, HTTPException
from starlette.responses import RedirectResponse

from inboxes import schema
from inboxes.domain import exceptions
from inboxes.repository import DummyRepository
from inboxes.services import generate_tripcode, confirm_tripcode

app = FastAPI()
repository = DummyRepository()


@app.get("/", include_in_schema=False)
def main():
    return RedirectResponse(url="/docs/")


@app.get("/inboxes", response_model=List[schema.Inbox])
def list_inboxes():
    result = repository.all()
    return result


@app.post("/inboxes", response_model=schema.Inbox)
def create_inbox(inbox: schema.InboxCreate, user: schema.UserCredentials):
    signature = generate_tripcode(**user.dict())
    inbox = repository.create(inbox, signature)
    return inbox


@app.get("/inboxes/{inbox_id}", response_model=schema.InboxDetail)
def inbox_detail(inbox_id: str, user: schema.UserCredentials):
    try:
        inbox = repository.detail(inbox_id)
    except exceptions.InboxNotFound:
        raise HTTPException(status_code=404, detail="Item not found")

    if not confirm_tripcode(user.username, user.password, inbox.signature):
        raise HTTPException(status_code=403, detail="Forbidden")

    return inbox


@app.patch("/inboxes/{inbox_id}", response_model=schema.Inbox)
def inbox_topic_update(inbox_id: str, new_topic: str, user: schema.UserCredentials):
    inbox = inbox_detail(inbox_id, user)
    try:
        inbox = repository.update_topic(inbox.id, new_topic=new_topic)
    except exceptions.InboxInProgressException:
        raise HTTPException(status_code=422, detail="Inbox has some answers already")

    return inbox


@app.post("/inboxes/{inbox_id}/answer")
def answer(inbox_id: str, message: schema.MessageCreate, user: Optional[schema.UserCredentials]):
    signature = generate_tripcode(**user.dict())
    try:
        message = repository.answer(inbox_id, message, signature)
    except exceptions.InboxNotFound:
        raise HTTPException(status_code=404, detail="Item not found")
    except exceptions.NoSignatureException:
        raise HTTPException(status_code=400, detail="No signature provided for non-anonymous inbox")
    except exceptions.InboxExpiredException:
        raise HTTPException(status_code=400, detail="Inbox already expired")
    return message
