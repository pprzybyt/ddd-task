import datetime
import pytz
from typing import Optional, List

from pydantic import BaseModel, validator


class MessageCreate(BaseModel):
    body: str


class Message(MessageCreate):
    signature: Optional[str]
    timestamp: Optional[datetime.datetime]

    @validator('timestamp')
    def set_timestamp(cls, timestamp):
        return timestamp or datetime.datetime.now(pytz.UTC)

    class Config:
        validate_assignment = True


class InboxCreate(BaseModel):
    topic: str
    is_anonymous: bool
    expiry_date: datetime.datetime


class Inbox(InboxCreate):
    id: str
    signature: str


class InboxDetail(Inbox):
    messages: List[Message]


class UserCredentials(BaseModel):
    username: str
    password: str
