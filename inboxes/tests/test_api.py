import json
from datetime import datetime
from unittest import TestCase

from fastapi.testclient import TestClient

from ..application import app
from .. import schema
from ..services import generate_tripcode

client = TestClient(app)


class TestApi(TestCase):

    def test_create_inbox(self):
        response = client.post('/inboxes')
        self.assertTrue(response.status_code == 422)
        user = schema.UserCredentials(username='user', password='user')
        inbox = schema.InboxCreate(topic='topic', is_anonymous=True, expiry_date=datetime.now())

        response = client.post('/inboxes',
                               json={
                                   'user': json.loads(user.json()),
                                   'inbox': json.loads(inbox.json())
                               })

        self.assertTrue(response.status_code == 200)
        self.assertTrue(schema.InboxCreate.parse_obj(response.json()) == inbox)
        self.assertTrue(response.json()['signature'] == generate_tripcode(user.username, user.password))

        response = client.get('/inboxes')
        self.assertTrue(response.status_code == 200)
        self.assertTrue(len(response.json()) == 1)
