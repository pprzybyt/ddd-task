from unittest import TestCase
from ..services import confirm_tripcode, generate_tripcode


class TestTripcodes(TestCase):

    def test_generate_tripcode(self):
        username = 'user'
        password = 'pass'
        separator = '#'
        tripcode = generate_tripcode(username, password, separator)
        self.assertTrue(tripcode.startswith(username + separator))

    def test_confirm_tripcode(self):
        username = 'user'
        password = 'pass'
        separator = '#'
        tripcode = generate_tripcode(username, password, separator)

        self.assertTrue(confirm_tripcode(username, password, tripcode))
        self.assertTrue(not confirm_tripcode(username, 'wrong-pass', tripcode))

