FROM python:3.9-slim

WORKDIR /code
COPY . /code/

RUN pip install --upgrade pip \
 && pip install -r requirements.txt

CMD uvicorn inboxes.application:app --host ${HOST} --port ${PORT} --reload

