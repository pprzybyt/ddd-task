fastapi==0.70.0
pydantic==1.8.2
pytz==2021.3
requests==2.26.0
starlette==0.16.0
uvicorn==0.15.0